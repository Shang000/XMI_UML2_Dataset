﻿# XMI_UML_Dataset

This dataset contains XMI documents that describe a UML model using eclipse UML2 namespace.
For the creation of the dataset, I did the following steps:

 1. Search for all .uml files which is written in XML using Github REST API which are greater than 10k (and less than 384k due to API search restriction). Criteria as command: extension:uml + language:xml + size:>10
 2. Remove all files that are not XMI documents, i.e. files that don't start with "<xmi:XMI" tag. There are some UML models that are only in XML and use xmi namespace. Those models will not be selected as they might not fully conform to XMI specification.
 3. Remove all files that do not use Eclipse UML2 namespace, i.e. the XMI document must contain: "xmlns:uml="http://www.eclipse.org/uml2/*" in its root node.
 4. Remove all files that have less than (including) 6 versions in the repository.
 5. Remove all eclipse UML clones such as Ecore.uml, UML.uml, Ecore.profile.uml.   
 6. Remove all files that are not UML:Model, like UML:Profile, UML:Class, or UML:Package
 7. The remaining files are kept in the dataset and all its history versions are downloaded. There is no guarantee that the dataset is matched (each element in the document have a unique xmi:id), but most do as recommended in the XMI specification. The name of the folders contain the name of the repository (separated by dot ".") and the directory of the file (separated by underscore "_"). 
 8. I have noticed that some of the earlier versions are not XMI documents, but XML documents using XMI namespace, I didn't remove these older versions to retain the completeness of the history.
 9. I also noticed that some of the earlier commits have a valid sha, but cannot be downloaded (404 Error). I didn't delete these folders for the completeness of the history, so that the number of versions in each file should exactly match the number of commits in the repository. But some version folders will be empty due to failure of retrieving the document.

There are 123 documents listed.
I have not checked all the models with Eclipse to see if they can be opened and EMF based, so I will have to check them one by one to see it.
If the documents are EMF based as expected and matched, then we can use diff tools like EMF Compare to find the diff and SCG (EMF Compare has it's own way to match elements if they don't have ids).
These XMI documents might also be used to generate diff XMI documents only based on XMI without considering meta-model.

Many of the XMI documents cannot pass the Eclipse validation check, the reasons of which are yet to be identified. 

